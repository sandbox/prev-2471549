<?php

	
	// Create permissions for module
	
	function node_booking_permission() {
		return array(
  			'node_booking' => array(
   			 'title' => t('Access course booking page'),
   			 'description' => t('Allows user to create/edit/delete course bookings.'),
	  		),
		);
	}


	function node_booking_admin_link(){
		drupal_goto('admin/config/content/node_booking');
	}
	
	function node_booking_admin() {
		  
		$form = array();

		$typeOptionArray = array();

		foreach(node_type_get_types() as $type) {
			$typeOptionArray[$type->type] = $type->name;
		}

		// Affiliated content types

		$form['enabled_for_content_types'] = array(
			'#type' => 'select',
			'#title' => t('Enabled Content Types'),
			'#default_value' => variable_get('enabled_for_content_types'),
			'#options' => $typeOptionArray,
			'#description' => t("Content types with booking option"),
			'#multiple' => TRUE
		);

		// Environment

		$form['environment'] = array(
			'#type' => 'select',
			'#title' => t('Environment'),
			'#default_value' => variable_get('environment'),
			'#options' => array(0 => 'Testing', 1 => 'Live' ),
			'#description' => t(''),
			'#multiple' => FALSE 
		);

		// Paypal credentials

		$form['paypal_id'] = array(
		  '#type' => 'textfield', 
		  '#title' => t('Paypal ID'), 
		  '#default_value' => variable_get('paypal_id'), 
		  '#size' => 60, 
		  '#maxlength' => 128, 
		);

		$form['paypal_test_id'] = array(
		  '#type' => 'textfield', 
		  '#title' => t('Paypal Test ID'), 
		  '#default_value' => variable_get('paypal_test_id'), 
		  '#size' => 60, 
		  '#maxlength' => 128, 
		);

		// Email contact

		$form['nb_admin_email'] = array(
		  '#type' => 'textfield', 
		  '#title' => t('Admin email'), 
		  '#default_value' => variable_get('nb_admin_email'), 
		  '#size' => 60, 
		  '#maxlength' => 128,
		  '#required' => true 
		);

		return system_settings_form($form);
			
	}


?>