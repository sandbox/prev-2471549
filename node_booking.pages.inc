<?php

	/* Node booking main page */

	function node_booking_page_nodes() {

		$page = '';

		$header = array(
			'course_name' => 'Node', 
			'status' => 'Page Status',
			'price' => 'price',
			'places' => 'Total Places',
			'bookings' => 'Bookings',
			'available' => 'Places Available', 
			// 'active' => array('data' => 'Booking Status', 'field' => 'active', 'order' => 'asc'),
			'active' => 'Booking Status',
			'operations' => 'Operations',
		);

		$clientUrl = 'admin/node_booking/manage/bookings/';

		$removeUrl = "admin/node_booking/confirm_remove/";

		$destArray = array('destination' => 'admin/node_booking/manage');

		$rows = array();

		$seperator = '&nbsp;|&nbsp;';

		foreach(get_all_nodes() as $id=>$record) {

			$rows[$id] = array(
				'course_name' => $record->title,
				'status' => $record->status ? 'Published' : 'Unpublished',
				'price' => money_format('$%i', $record->price),
				'places' => $record->places,
				'bookings' => booking_count($record->nid),
				'available' => $record->places - booking_count($record->nid),
				'active' => $record->active ? 'ACTIVE' : 'CLOSED',
				'operations' => 
						l('Bookings', $clientUrl.$record->nid).$seperator.
						l('Edit', 'node/'.$record->nid.'/edit', array('query' => $destArray)).
						$seperator.l('Remove', $removeUrl.$record->nid)
			);
		}
		

		$per_page = 10;
	
		$current_page = pager_default_initialize(count($rows), $per_page);
	
		$chunks = array_chunk($rows, $per_page, TRUE);

		$page .= theme('table', array('header' => $header, 'rows' =>  $chunks[$current_page], 'sticky' => TRUE, 'empty' => 'No node_booking currently activated'));

		$page.= theme('pager', array('quantity',count($rows)));

		return $page;

		// $page .= theme('table', array('header' => $header, 'rows' => $rows, 'sticky' => TRUE, 'empty' => 'No node_booking currently activated'));

		// $page .= theme('pager');

		// return $page;
	}

	/* Booking page node filter form */

	function node_booking_node_filter_form_submit($form, &$form_state){

		$page = '';

		$nid = $form_state['values']['nodes'];

		drupal_goto('admin/node_booking/manage/bookings/' . $nid );

		return $page;

	}

	function node_booking_node_filter_form($form, &$form_state, $selected_nid) {
		
		$form = array();

		$options = array();

		foreach(get_all_nodes() as $record) {

			$options[$record->nid] = $record->title;

		}

     	$form['select-node'] = array(
     		'#title' => 'Filter by node',
     		'#type' => 'fieldset',
     	);

		$form['select-node']['nodes'] = array(
			'#type' => 'select',
			'#title' => t('Select Node'),
			'#default_value' =>  $selected_nid ? $selected_nid : '',
			'#options' => $options,
			'#description' => 'Select a node to display current bookings'
		);

		$form['select-node']['submit'] = array(
	        '#type' => 'submit',
	        '#value' => t("Filter"),
		);

		return $form;
	}

	/* Node booking booking page */

	function node_booking_page_bookings($arg1 = null) {

		$page = '';

		// $page .= 'booking count ' . check_bookings($arg1);

		$filter_form = drupal_get_form('node_booking_node_filter_form', $arg1);

		$page .= drupal_render($filter_form);

		if($arg1 == null) {

			drupal_set_message('Please specify a node', 'warning');

			return $page;

		}

		$node_record = get_single_record('node_booking_nodes', 'nid', $arg1);

		if(!$node_record) {

			drupal_set_message('Please specify a node', 'warning');
			
			return $page;
		}

		$available = places_available($arg1);

		$page .= t('<h4>Current bookings for \'' . get_node_title($arg1) . '\' - ' . $available . ' place' .
			($available == 1 ? '' : 's') .' available</h4>');

		$rows = array();
		
		$header = array(
			'bid' => 'Booking ID',
			'fname' => 'First Name',
			'lname' => 'Surname', 
			'email' => 'Email Address',
			'date' => 'Payment Date',
			'ipn_track_id' => 'IPN ID',
			// 'active' => array('data' => 'Status', 'field' => 'active', 'order' => 'asc'),
			'operations' => 'Operations',
		);

		$result = get_clients($arg1);

		foreach($result as $id=>$record) {

			$rows[$id] = array(
				'bid' => $record->bid,
				'fname' => $record->client_first_name,
				'lname' => $record->client_last_name,
				'email' => $record->client_email,
				'date' => date('H:i d M Y', $record->payment_date),
				'ipn_track_id' => $record->ipn_track_id,
				// 'active' => $record->active ? 'ACTIVE' : 'CLOSED',
				'operations' => l('Remove Booking', 'admin/node_booking/remove_booking/'.$record->bid)
			);
		}
		
		$page .= theme('table', array('header' => $header, 'rows' => $rows, 'sticky' => TRUE, 'empty' => 'No clients for this course'));

		$page .= '<br>';

		$dest = 'admin/node_booking_manage_bookings/' . $arg1;

		$destArray = array('destination' => $dest);

		$page .= l('Export .CSV', 'admin/node_booking/manage/bookings/export/' . $arg1, array('attributes'=>array('class' => 'button', 'style' => ''), 'query' => $destArray));



		return $page;

	}

	function node_booking_remove_booking($arg_1, $arg_2, $bid) {
		drupal_set_message('Removed Booking with BID ' . $bid);
		$num_deleted = db_delete('node_booking_bookings')
  		->condition('bid', $bid)
  		->execute();
  		drupal_goto('admin/node_booking/manage/bookings');
	}

	function node_booking_confirm_remove($arg1, $arg2, $nid) {

		if($_GET['status'] == 'confirmed') {
	  		node_booking_remove($nid);
		} else {
			$page = '';
			$page .= '<a href="/admin/node_booking/confirm_remove/' . $nid . '?status=confirmed">Click here to remove all booking data associated with \''. get_node_title($nid) . '\'</a>';
			return $page;
		}
	}

	function node_booking_remove($nid) {
		drupal_set_message('Removed booking info from node ' . get_node_title($nid));
		$num_deleted = db_delete('node_booking_nodes')
  		->condition('nid', $nid)
  		->execute();
  		
		drupal_set_message('Removed Bookings from node ' . get_node_title($nid));
		$num_deleted = db_delete('node_booking_bookings')
  		->condition('nid', $nid)
  		->execute();

  		drupal_goto('admin/node_booking/manage');
	}


	function node_booking_page_clients($arg1 = null) {
		
		$page = '';

		$client_search = "<p><input type='text' value='Client search coming soon...' class='form-text' ></p>";

		$page .= $client_search;

		$header = array(
			
			// 'paypal_id' => 'Paypal ID,
			// 'paypal_status',
			'cid' => 'Client ID',
			'client_first_name' => 'First Name',
			'client_last_name' => 'Surname',
			'client_email' => 'Email',
			
			// 'address_country' => 'Country',
			// 'address_country_code' ,
			// 'address_name' => 'Address 1',

			'address_street' => 'Street',
			'address_city' => 'City',
			'address_state' => 'State',
			// 'address_status' => 'Address 3',
			'address_zip' => 'Zip',

			// 'fname' => 'First Name',
			// 'lname' => 'Surname', 
			// 'email' => 'Email Address',
			// 'landline' => 'Landline',
			// 'mobile' => 'Mobile',
			// 'active' => array('data' => 'Status', 'field' => 'active', 'order' => 'asc'),
			// 'operations' => 'Operations',
		);

		$rows = array();

		foreach(get_all_records('node_booking_clients') as $id => $record) {

			$rows[$id] = array(
				'cid' => $record->cid,
				'client_first_name' => $record->client_first_name,
				'client_last_name' => $record->client_last_name,
				'client_email' => $record->client_email,
				'address_street' => $record->address_street,
				'address_city' => $record->address_city,
				'address_state' => $record->address_state,
				'address_zip' => $record->address_zip,
				
				// 'active' => $record->active ? 'ACTIVE' : 'CLOSED',
				// 'operations' => l('Remove Client', '')
			);
		}
		
		$per_page = 20;
	
		$current_page = pager_default_initialize(count($rows), $per_page);
	
		$chunks = array_chunk($rows, $per_page, TRUE);

		$page .= theme('table', array('header' => $header, 'rows' =>  $chunks[$current_page], 'sticky' => TRUE, 'empty' => 'No clients for this course'));

		$page.= theme('pager', array('quantity',count($rows)));

		return $page;

	}



	function node_booking_confirmpayment_page($arg1 = null, $arg2 = null) {
		$page = '';
		if($arg2) {
			
			// Check if node has bookings and is active

			if(!places_available($arg2) || !node_booking_node_is_active($arg2)) {
				$page .= '<p><strong>Sorry, there are currently no places available.</strong></p>';
				return $page;
			}

			$nid = intval($arg2);
			$title = get_node_title($nid);
			$price_int = get_single_record('node_booking_nodes', 'nid', $nid)->price;
			$price = money_format('$%i', $price_int);
			$page = '<h3>Click below to proceed with payment of ' . $price . ' for ' . 
				$title . '</h3><p class="subtext"> (Credit card or PayPal account accepted)</p>';
			$payment_form = drupal_get_form('node_booking_paypal_form', $price_int, $title, $arg2);
			$page .= drupal_render($payment_form);

		} else {
			drupal_set_message('I&rsqo;m sorry, that url is invalid');
			
		}

		return $page;
	}

?>