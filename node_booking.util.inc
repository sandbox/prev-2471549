<?php

	/* Utility functiions */

	function check_type($type) {
    	return in_array($type, variable_get('enabled_for_content_types'));
    }

    function node_has_record($nid) {
    	return db_query("SELECT 1 FROM {node_booking_nodes} WHERE nid = :nid", array(':nid' => $nid))->fetchField();
    }

    function client_exists($email) {
    	return db_query("SELECT cid FROM {node_booking_clients} WHERE client_email = :email", array(':email' => $email))->fetchField(0);
    }

    function get_latest_client() {
    	return db_query("SELECT MAX(cid) FROM {node_booking_clients}")->fetchField();
    }

    function node_booking_node_is_active($nid) {
    	return db_query("SELECT active FROM {node_booking_nodes} WHERE nid = :nid", array(':nid' => $nid))->fetchField();
    }

    function places_available($nid) {
    	$places = db_query("SELECT places FROM {node_booking_nodes} WHERE nid = :nid", array(':nid' => $nid))->fetchField();

    	$available = $places - booking_count($nid);

    	return $available;
    }

    function booking_count($nid) {
    	return db_query("SELECT 1 FROM {node_booking_bookings} WHERE nid = :nid", array(':nid' => $nid))->rowCount();
    }


    function get_clients($nid) {

    	$rows = array();

		$query = db_select('node_booking_clients', 'cl');

		$query->leftjoin('node_booking_bookings', 'b', 'b.cid = cl.cid');
	
		$query->fields('cl');
		$query->fields('b');

		$query->condition('b.nid', $nid, '=');

		return $query->execute();
    }

    function get_single_record($table, $column, $record_id) {

		$rows = array();

		$query = db_select($table, 't');

		$query->fields('t');

		$column_ref = 't.'.$column;

		$query->condition($column_ref, intval($record_id), '=');

		$result = $query->execute();

		foreach($result as $record) {
			return $record;
		}

		return;

	}

    function get_all_nodes() {

    	$rows = array();

		$query = db_select('node_booking_nodes', 'nb');
		
		$query->leftjoin('node', 'n', 'nb.nid = n.nid');
	
		$query->fields('n');

		$query->fields('nb');

		return $query->execute();

    }

    function get_all_records($table) {

    	$rows = array();

		$query = db_select($table, 't');
	
		$query->fields('t');

		return $query->execute();
    }


?>