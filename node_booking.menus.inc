<?php
	// Setup menus
	
	function node_booking_menu() {
	  	
	  	$items = array();


		$items['admin/config/content/node_booking'] = array(
			'title' => 'Node Booking Settings',
			'description' => 'Manage settings for the node booking module',
			'page callback' => 'drupal_get_form',
			'access arguments' => array('administer site configuration'),
			'page arguments' => array('node_booking_admin'),
			'access callback' => TRUE,
		);

		$items['admin/node_booking/manage'] = array(
			'title' => 'Node Bookings',
			'description' => 'Manage node bookings',
			'page callback' => 'node_booking_page_nodes',
			'access arguments' => array('node_booking'),
			'type' => MENU_NORMAL_ITEM,
		);


		$items['admin/node_booking/manage/nodes'] = array(
		    'title' => 'Nodes',
		    'type' => MENU_DEFAULT_LOCAL_TASK,
		    'weight' => 0
		);

		$items['admin/node_booking/manage/clients'] = array(
			  'title' => 'All Clients',
			  'description' => 'Manage node booking clients',
			  'page callback' => 'node_booking_page_clients',
			  'access arguments' => array('node_booking'),
			  'weight' => 2,
			  'type' => MENU_LOCAL_TASK
		);

		$items['admin/node_booking/manage/bookings'] = array(
			  'title' => 'Bookings',
			  'description' => 'Manage node bookings',
			  'page callback' => 'node_booking_page_bookings',
			  'access arguments' => array('node_booking'),
			  'weight' => 1,
			  'type' => MENU_LOCAL_TASK
		);

		$items['admin/node_booking/remove_booking/%'] = array(
			  'page callback' => 'node_booking_remove_booking',
			  'access arguments' => array('node_booking'),
			  'page arguments' => array(1, 2, 3),
			  'type' => MENU_CALLBACK,
		);


		$items['admin/node_booking/confirm_remove/%'] = array(
			'title' => 'Confirm removal',
			'page callback' => 'node_booking_confirm_remove',
			'access arguments' => array('node_booking'),
			'page arguments' => array(1, 2, 3),
			'type' => MENU_CALLBACK,
		);

		$items['admin/node_booking/manage/bookings/export'] = array(
			'title' => 'Export as csv',
			'description' => 'Export',
			'tab parent' => 'admin/node_booking/manage/bookings',
			'page callback' => 'node_booking_export_handler',
			'access arguments' => array('node_booking'),
			'weight' => 1,
			'type' => MENU_CALLBACK
		);

		$items['admin/node_booking/manage/nodes/settings'] = array(
			'title' => 'Settings',
			'description' => 'Node Booking settings',
			'page callback' => 'node_booking_admin_link',
			'access arguments' => array('node_booking'),
			'weight' => 1,
			'type' => MENU_LOCAL_ACTION
		);

		$items['node_booking/confirm-payment/%'] = array(
			  'title' => 'Confirm payment',
			  'description' => 'Conifrm booking payment',
			  'page callback' => 'node_booking_confirmpayment_page',
			  'access arguments' => array('access content'),
			  'page arguments' => array(1, 2),
			  'type' => MENU_CALLBACK,
		);

		$items['node_booking/confirmation'] = array(
			  'title' => 'Process payment message',
			  'description' => 'Process payment message',
			  'access arguments' => array('access content'),
			  // 'access callback' => TRUE,
			  'page callback' => 'node_booking_payment_handler',
			  'type' => MENU_CALLBACK,
		);


		return $items;
	}

?>